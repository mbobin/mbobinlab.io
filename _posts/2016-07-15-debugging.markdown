---
layout: single
title:  "Debugging"
date:   2016-07-15 19:45:30 +0300
categories: linux ruby bash
excerpt: 'A useful list of commands for debugging stuck processes'
---

## [lsof](http://manpages.ubuntu.com/manpages/precise/en/man8/lsof.8.html) - List open files.

List information about files opened by processes on a UNIX box.

{% highlight bash %}
  $> lsof -p <pid1,pid2>
{% endhighlight %}

Include details about `pid1` and `pid2`.

{% highlight bash %}
  $> lsof -p <pid1,pid2,^pid3>
{% endhighlight %}

Exclude `pid3`

{% highlight bash %}
  $> lsof -c ruby
{% endhighlight %}

Files opened by all processes executing a command starting with `ruby`.

{% highlight bash %}
  $> lsof /path/to/file
{% endhighlight %}

List processes that have the `/path/to/file` file open.

{% highlight bash %}
lsof /dev/sda1
{% endhighlight %}

What files are keeping `/dev/sda1` busy.

{% highlight bash %}
  $> lsof -i:50-5000
{% endhighlight %}

All networking related to ports `50-5000`

{% highlight bash %}
  $> lsof -i -a -c ruby
{% endhighlight %}

Inspect Internet connections for Ruby processes. `-a` is used to `AND` selections.

{% highlight bash %}
  $> lsof -r -c ruby
{% endhighlight %}

Puts `lsof` in repeat mode. There `lsof` lists open files as selected by other options, delays `t` seconds (default fifteen), then repeats the listing, delaying and listing repetitively until stopped by a condition defined by the prefix to the option.

If the prefix is a `-`, repeat mode is endless and `lsof` must be terminated with an interrupt or quit signal.

If the prefix is `+`, repeat mode will end the first cycle no open files are listed and of course `lsof` is stopped with an interrupt or quit signal. When repeat mode ends because no files are listed, the process exit code will be zero if any open files were ever listed; one, if none were ever listed.

{% highlight bash %}
  $> lsof -t -i:3000 | xargs -n 1 kill -9
{% endhighlight %}

Specifies that `lsof` should produce terse output with process identifiers only and no header. The output is piped to kill.

## [strace](http://manpages.ubuntu.com/manpages/precise/en/man1/strace.1.html)

Trace system calls and signals.

{% highlight bash %}
  $> irb
  $> strace -o strace.log -p <irb_pid>
  irb> require 'nokogiri'
{% endhighlight %}

See where a library was loaded and what its associated directory lookup order is.

### Stare at the code until it makes sense.

- [Troubleshooting Ruby Processes: Leveraging System Tools when the Usual Ruby Tricks Stop Working](https://www.safaribooksonline.com/library/view/troubleshooting-ruby-processes/9780321544681)
- [gdb](http://manpages.ubuntu.com/manpages/precise/en/man1/gdb.1.html)
- [Debugging with gdb](https://sourceware.org/gdb/current/onlinedocs/gdb/index.html#Top)
- [rpm/bin/nrdebug](https://github.com/newrelic/rpm/blob/master/bin/nrdebug)
