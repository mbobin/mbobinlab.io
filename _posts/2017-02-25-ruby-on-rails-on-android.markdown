---
layout: single
title:  "Ruby on Rails on Android"
date:   2017-02-25 14:20:09 +0200
categories: ruby
excerpt: "Running a Ruby on Rails web application on a mobile device"
---

I have fooled around with [Ruboto](http://ruboto.org/) but I don't want to build native Android apps. I just want to start an irb session without 3 apps.

Here comes [Termux](http://termux.com). This is its official description:

> Termux is an Android terminal emulator and Linux environment app that works directly with no rooting or setup required. A minimal base system is installed automatically - additional packages are available using the APT package manager.

The definition of a gold mine if you ask me. And yeah, they have a lot of packages, including the latest stable version of Ruby. Go and see for yourself on their github [repository](https://github.com/termux/termux-packages).

Get it from [Google Play](https://play.google.com/store/apps/details?id=com.termux), update the packages list and install Ruby in one line.

{% highlight bash %}
$ apt update
$ apt install ruby
$ ruby -v
ruby 2.4.0p0 (2016-12-24 revision 57164) [aarch64-linux-android] # or something like this
{% endhighlight %}


Formidable! Now let's put it to the test.

{% highlight ruby %}
irb> puts "Hello World!"
Hello World!
=> nil
{% endhighlight %}

{% highlight bash %}
$ mkdir -p projects/ruby
$ echo 'puts "hello world!"' > projects/ruby/hello.rb
$ ruby projects/ruby/hello.rb
hello world!
{% endhighlight %}

Isn't that wonderful?

I have a [OnePlus 2](https://oneplus.net/uk/2) and 2 GiB of RAM are free most of the time. That's much more than you can get from Heroku's free dyno.
Let's see if we can run a Rails app on Android.

First step: bundler

{% highlight bash %}
$ gem install bundler
{% endhighlight %}

Now for the painful part, installing nokogiri's dependencies.

We need something to build our native extensions.

{% highlight bash %}
$ apt install clang make
{% endhighlight %}

And these should do the trick:

{% highlight bash %}
$ apt install ruby-dev pkg-config libxml2 libxml2-dev libxslt libxslt-dev
$ gem install pkg-config
{% endhighlight %}

Say a prayer and hit return after the following line.

{% highlight bash %}
$ gem install nokogiri -- --use-system-libraries
{% endhighlight %}

Good. I spent quite some time gathering all of them (more than I care to admit as a matter of fact).

We will need a database for our app. Rails comes by default with a sqlite database  and it carries along some dependencies that are not satisfied by default on our android device.

{% highlight bash %}
$ apt install libffi libffi-dev
$ apt install libsqlite libsqlite-dev
{% endhighlight %}

And something for our asset pipeline:

{% highlight bash %}
$ apt-install nodejs
{% endhighlight %}

Our reward:

{% highlight bash %}
$ gem install rails -v 5.2
$ cd projects/ruby
$ rails new demo
$ cd demo
{% endhighlight %}

We need to add a couple of gems for timezones. Open Gemfile with vi and add(be carefull with their order and platform options if any):

{% highlight ruby %}
gem 'tzinfo'
gem 'tzinfo-data'
{% endhighlight %}

The moment of truth:

{% highlight bash %}
$ bundle install
$ rails server
{% endhighlight %}

![It's alive!](/assets/rails_on_android.jpg "It's alive!!!")

You can find a working demo app on my [GitHub](https://github.com/mbobin/ruby-on-rails-android-demo) profile.
 

:-------------------------:|:-------------------------:
![Rails main screen](/assets/Screenshot_20190114-204007.jpg) | ![Rails main screen logs](/assets/Screenshot_20190114-204001.jpg)
![Scaffold resource](/assets/Screenshot_20190114-204215.jpg) | ![Scaffold resource logs](/assets/Screenshot_20190114-204230.jpg)

![Rails console](/assets/Screenshot_20190114-205543.jpg)

> update 14.01.2018: Updated Rails version and added demo app and pictures.
