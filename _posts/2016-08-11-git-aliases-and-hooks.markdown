---
layout: single
title:  "Git: aliases and hooks"
date:   2016-08-11 21:44:15 +0300
categories: git bash linux
excerpt: "Git aliases and hooks - sharp tools for everyday use"
---

## Git aliases

Open `~/.gitconfig` with your favorite text editor.

{% highlight bash %}
vim ~/.gitconfig
{% endhighlight %}

And paste the following config.

{% highlight git %}
[alias]
  st = status
  ci = commit
  co = checkout
  br = branch
  history = "!git log --pretty=format:\"%Cblue%ad%Creset : %an :%C(yellow)%d %Creset:%Cgreen %s \" --reverse --all --since=1.month.ago --author=\"$(git config user.name)\""
  undo = reset --soft HEAD~1
{% endhighlight %}

They are basically shortcuts for the common git commands.

* `git history`: It shows last month's commits for the current user, or as my one of my friends call it git toggl.
* `git undo`: Who doesn't screw up a commit? wrong files, wrong message, endless possibilities...


## Git hooks

| Hooks are programs you can place in a hooks directory to trigger actions at certain points in git’s execution. - [docs](https://git-scm.com/docs/githooks)

Make a new directory in home to store them.

Download a pre-commit file. It contains a scrap of code which won't let you commit files with debuggers.
It works for Ruby/Ruby on Rails projects by scanning each line for debuggers. It will let you know if you forgot something.


{% highlight bash %}
mkdir -p ~/.git-hooks
wget --output-document ~/.git-hooks/pre-commit https://gist.githubusercontent.com/mgb313/7bd00aaffe5085830127a56ef7ec1926/raw/1fa454b07c41e4a35b47c47597e6eba02632b7f8/pre-commit
chmod +x ~/.git-hooks/pre-commit
{% endhighlight %}

Tell git where to find your hooks:

{% highlight git %}
[core]
  hooksPath = ~/.git_hooks
  excludesfile = ~/.gitignore_global
{% endhighlight %}
Now every time call to `git commit` will be checked against those patterns.
If you want to commit your changes anyway, use `git commit --no-verify` to bypass the pre-commit and commit-msg hooks.

You can also add a `.gitignore` file to be used globally.

