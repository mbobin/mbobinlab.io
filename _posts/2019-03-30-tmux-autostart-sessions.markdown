---
layout: single
title: Tmux - autostart sessions
date: '2019-03-30 23:10:18 +0200'
categories: linux bash tmux
excerpt: 'How to start Tmux automatically on each bash session'
---

{% highlight bash %}
if (command -v tmux>/dev/null) && \
  [[ ! $TERM =~ screen ]] && \
  [ -z $TMUX ] && \
  [ "$TMUX_ENABLED" == "yes" ]; then
    /usr/bin/env tmux new -s 0
    /usr/bin/env tmux attach -t 0
fi
{% endhighlight %}

Breakdown:
  - `(command -v tmux>/dev/null)` - Tmux is installed
  - `[[ ! $TERM =~ screen ]] && [ -z $TMUX ]` - try not to run Tmux inside Tmux
  - `[ "$TMUX_ENABLED" == "yes" ]` - TMUX_ENABLED is set from [Alacritty](https://github.com/jwilm/alacritty) config file and it will start Tmux only in this terminal emulator.
  - `tmux new -s 0` - find or duplicate a session with name 0
  - `tmux attach -t 0` - attach to session 0
