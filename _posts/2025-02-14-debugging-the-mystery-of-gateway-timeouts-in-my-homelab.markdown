---
layout: single
title: Debugging the mystery of gateway timeouts in my homelab
date: '2025-02-14 17:15:30 +0200'
categories: docker
excerpt: 'How to fix gateway timeouts for Traefik with a Docker provider'
---

Ever wondered why your perfectly configured homelab suddenly starts throwing tantrum-like gateway timeouts? I recently solved a mysterious issue in my Docker-based homelab that had been driving me crazy, and I thought I'd share the journey.

My homelab setup is intentionally straightforward - Docker Compose paired with Traefik for reverse proxy duties. I chose this over Kubernetes because, let's face it, sometimes simpler is better,  especially when you're trying to debug issues from your phone while sitting on the couch watching TV.

Each application in my setup lives in its own Docker Compose file, with Traefik handling the routing and SSL certificates. Everything was running smoothly until it wasn't. It started with occasional gateway timeouts in Synapse, and when I added Immich to the mix, things got even more interesting (and by interesting, I mean frustrating).

The plot thickened when I bypassed Traefik and accessed the services directly. Surprisingly, everything worked fine! This was my first clue that Traefik was the culprit, not the services themselves.
Here's where it gets interesting: my containers were connected to two Docker networks:

- A `backend` network for inter-container communication within each service
- A `frontend` network shared by all HTTP-serving containers for Traefik routing

What I didn't realize (and what caused all those headaches) was that Traefik plays a game of network roulette when containers have multiple networks. It randomly picks one to route traffic through. So while initial testing might work perfectly, a few hours later you might find yourself staring at a gateway timeout error because Traefik decided to use a different network.

The solution turned out to be quite simple and possible obvious if I had read the full documentation. Traefik has a [setting](https://doc.traefik.io/traefik/routing/providers/docker/#traefikdockernetwork) that lets you specify which network it should use. Just add this label to your container configuration:

```yaml
- "traefik.docker.network=mynetwork"
```

And just like that, no more gateway timeouts! Traefik now consistently uses the network you specify, bringing stability back to your homelab.
Remember folks: when dealing with multi-network containers, don't leave routing to chance. Tell Traefik exactly which network to use, and save yourself some troubleshooting headaches.
