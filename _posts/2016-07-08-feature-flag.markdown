---
layout: single
title:  "Feature flag"
date:   2016-07-08 19:45:30 +0300
categories: ruby
excerpt: 'Change Ruby code during runtime'
---

Change Ruby code during runtime

{% highlight ruby %}
class Document
  attr_accessor :text

  def self.toggle_encryption(state: true)
    if state
      def encrypt
        StrongEncryptor.call text
      end
    else
      def encrypt
        Encryptor.call text
      end
    end
  end
  toggle_encryption
end
{% endhighlight %}

Calling `Document.toggle_encryption(state: true)` will change the instance method `encrypt` to use `StrongEncryptor` implementation. Similarly, calling it with `state: false` will change the implementation to use `Encryptor`. This is a naive, contrived example that will not work for multi-process production environments, but it is still useful for hacking and debugging.
