---
layout: single
title:  "Tmux configs"
date:   2018-10-07 00:52:09 +0300
categories: tmux bash linux
excerpt: "My tmux cheatsheet"
---

Key bindings from my [dotfiles](https://github.com/mbobin/dotfiles) for Tmux:

| tmux key | Description |
| --- | --- |
| `C-a` | Default prefix, used instead of "C-b". |
| `<prefix> a` | Move to the beginning of the line. |
| `<prefix> r` | Reload tmux configuration from ~/.tmux.conf file. |
| `<prefix> -` | Split new pane horizontally. |
| `<prefix> |` | Split new pane vertically. |
| `<prefix> h` | Select pane on the left. |
| `<prefix> j` | Select pane on the bottom. |
| `<prefix> k` | Select pane on the top. |
| `<prefix> l` | Select pane on the right. |
| `<prefix> →` | Select pane on the right. |
| `<prefix> ←` | Select pane on the left. |
| `<prefix> ↓` | Select pane on the bottom. |
| `<prefix> ↑` | Select pane on the top. |
| `<prefix> C-←` | Resize pane to the left. |
| `<prefix> C-→` | Resize pane to the right. |
| `<prefix> C-↑` | Resize pane to the top. |
| `<prefix> C-↓` | Resize pane to the bottom. |
| `<prefix> +` | Zoom in/out on the active pane. |
| `<prefix> I` | Synchronize panes from the current window. |
| `<prefix> c` | Create a new window. |
| `<prefix> n` | Switch to the next window. |
| `<prefix> S-→` | Switch to the next window. |
| `<prefix> p` | Switch to the previous window. |
| `<prefix> S-←` | Switch to the previous window. |
| `<prefix> 0-9` | Switch to a window using it’s index number. |
| `<prefix> C-a` | Switch to the last used window. |
| `<prefix> v` | Enter into Copy mode. |
| `<prefix> y` | Copy selection. |
| `<prefix> Enter` | Copy selection. |
