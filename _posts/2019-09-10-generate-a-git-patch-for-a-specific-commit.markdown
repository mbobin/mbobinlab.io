---
layout: single
title: Git patch cheatsheet
date: '2019-09-10 12:16:37 +0300'
categories: git bash
excerpt: 'Generate a git patch for a specific commit'
---

I always forget how to generate git patches and need to search for the right commands. This is a collection of useful commands and their source.

{% highlight bash %}
  git format-patch -1 <sha>
  git apply --stat file.patch # show stats.
  git apply --check file.patch # check for error before applying.
  git am < file.patch # apply the patch finally.
{% endhighlight %}

[source](https://mirrors.edge.kernel.org/pub/software/scm/git/docs/git-format-patch.html)

To generate the patches from the topmost commits from a specific `SHA1` hash:

{% highlight bash %}
git format-patch -<n> <SHA1>
{% endhighlight %}

The last 10 patches from `head` in a single patch file:

{% highlight bash %}
git format-patch -10 HEAD --stdout > 0001-last-10-commits.patch
{% endhighlight %}

[source](https://stackoverflow.com/questions/6658313/generate-a-git-patch-for-a-specific-commit)
