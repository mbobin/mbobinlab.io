---
layout: single
title:  "Pi-hole and OpenVPN setup"
date:   2018-10-15 22:01:09 +0300
categories: linux
excerpt: "Setup Pi-hole and OpenVPN on Raspberry Pi"
---

* [Setup Overview](https://docs.pi-hole.net/guides/vpn/overview/)
* [Dual operation: LAN & VPN at the same time](https://docs.pi-hole.net/guides/vpn/dual-operation/)
* [Configuring DNS-Over-HTTPS on Pi-hole with Cloudflare](https://docs.pi-hole.net/guides/dns-over-https/)
* [Enable Dynamic DNS for a domain with Namecheap](https://www.namecheap.com/support/knowledgebase/article.aspx/595/11)
* [DDclient - Dynamic DNS Client](https://sourceforge.net/p/ddclient/wiki/Home/)
* [Namecheap DDclient configuration](https://www.namecheap.com/support/knowledgebase/article.aspx/583/11/how-do-i-configure-ddclient)
