---
layout: single
title: Marius Bobin
permalink: /about/
author_profile: true
---

* Backend Engineer, Verify @ [GitLab](https://gitlab.com) since August, 2019
* TL @ [Sparktech](http://www.sparktech.ro) July, 2018 <-> July, 2019
* Ruby on Rails Backend Developer @ [Sparktech](http://www.sparktech.ro) August, 2015 <-> July, 2019
* Ruby on Rails Teacher @ [Faculty of Mathematics and Computer Science](http://fmi.unibuc.ro/), University of Bucharest, 2018 <-> 2019
* Ruby on Rails Mentor @ [DevAcademy](https://devacademy.ro), 2016 <-> 2017
* Developer @ [DevAcademy](https://devacademy.ro), 2013 <-> 2017
* Volunteer @ [ASMI](http://www.as-mi.ro/), 2012 <-> 2015
* Attended [Faculty of Mathematics and Computer Science](http://fmi.unibuc.ro/), University of Bucharest, 2012 <-> 2015 - Bachelor of Computer Science

As far as experience goes, I like to keep my boat afloat with various challenging tasks, designing the architecture for enterprise applications such as content and customer relationship management systems, online recruitment systems, online learning platforms or AI accelerator platforms on top of Kubernetes.

Some other ones worth mentioning are developing internal libraries and tools to support code reuse and faster development time when creating new applications and
designing and implementing applications' database architecture using meta-programming to reduce coding time and push the limits of ActiveRecord and PostgreSQL's JSONb data type.

I support my team by speeding up applications build time by leveraging Docker's layer caching, load time and improving tests running time and also assist with configuration, and maintenance of applications dependencies and databases.

I'm most comfortable in the Ruby land and trying my luck with Elixir and Go, but you'll find me thrilled when the situation allows replacing all of them with a BASH [one liner](http://www.bashoneliners.com/). Now I'm interested in simplifing the deployment process for Rails(running migrations is the thing that keeps comming back), monitoring and performance optimizations.

__Likes:__  ruby, rails, finally seeing something obvious for the first time, sql, daydreaming, elixir, long walks, linux, coming home, phoenix, nosql, crystal, mp3s, mkvs, postgres, snow, sidekiq, OSS, git, nomad, winter, kafka, ePUBs, GitLab, hacker news, ssh, running, kubernetes, flashes of insight, elasticsearch, GitHub, rsync, the inexplicable, bash, docker, post-adrenaline euphoria, nginx, hanami, swimming, passenger, terraform, faktory, xkcd, prometheus, tinkering, Gitlab CI, tmux, alacritty, sinatra, bikes, redis, commitstrip, grafana, dry-rb, air, alpine, mina, ops, screwdrivers, Pi, ansible.

__Dislikes:__ heat, loud noises, admin, js, big crowds, legalese, salary negotiations.

__Terrified by:__  public speaking.

__Programming books that I recommend:__

You will definitely find me with a couple of opened browser tabs on my laptop or mobile phone about what's happening in the programming world, ranging from blog posts, tutorials, podcasts to conference videos. I wish I had the time to go though all of them.

* [Practical Object-Oriented Design in Ruby](http://www.poodr.com/) by Sandi Metz
* [99 Bottles of OOP](https://www.sandimetz.com/99bottles/) by Sandi Metz and Katrina Owen
* [The Pragmatic Programmer](https://pragprog.com/book/tpp/the-pragmatic-programmer) by Andrew Hunt and David Thomas
* [Design Patterns in Ruby](http://designpatternsinruby.com/) by Russ Olsen
* [Confident Ruby](http://www.confidentruby.com/) by Avdi Grimm
* [Exceptional Ruby](http://exceptionalruby.com/) by Avdi Grimm
* [Metaprogramming Ruby 2](https://pragprog.com/book/ppmetr2/metaprogramming-ruby-2) by Paolo Perrotta
* [Effective Ruby](https://www.effectiveruby.com/) by Peter J. Jones
* [Eloquent Ruby](http://eloquentruby.com/) by Russ Olsen
* [Refactoring Ruby Edition](https://martinfowler.com/books/refactoringRubyEd.html) by Jay Fields, Shane Harvie, and Martin Fowler (with Kent Beck)
* [Ruby Best Practices](http://shop.oreilly.com/product/9780596523015.do) By Gregory Brown
* [Ruby Performance Optimization](https://pragprog.com/book/adrpo/ruby-performance-optimization) by Alexander Dymo
* [Troubleshooting Ruby Processes](https://www.informit.com/store/troubleshooting-ruby-processes-leveraging-system-tools-9780321544681) by Philippe Hanrigou
* [Rails AntiPatterns](http://www.informit.com/store/rails-antipatterns-best-practice-ruby-on-rails-refactoring-9780321604811) by Chad Pytel and Tammer Saleh
* [The Rails 5 Way](https://leanpub.com/tr5w) by Obie Fernandez
* [Trailblazer](https://leanpub.com/trailblazer) by Nick Sutterer
* [Effective Testing with RSpec 3](https://pragprog.com/book/rspec3/effective-testing-with-rspec-3) by Myron Marston and Ian Dees
* [Production-Ready Microservices](http://shop.oreilly.com/product/0636920053675.do) by Susan Fowler
* [Linux Shell Scripting Essentials](https://www.packtpub.com/networking-and-servers/linux-shell-scripting-essentials) by Sinny Kumari
* [Docker in Action](https://www.manning.com/books/docker-in-action) by Jeff Nickoloff
* [Docker Orchestration](https://www.packtpub.com/virtualization-and-cloud/docker-orchestration) by Randall Smith
* [Elasticsearch in Action](https://www.manning.com/books/elasticsearch-in-action) by Radu Gheorghe, Matthew Lee Hinman, and Roy Russo
* [Elasticsearch: The Definitive Guide](http://shop.oreilly.com/product/0636920028505.do) by Clinton Gormley and Zachary Tong
* [Études for Elixir](http://chimera.labs.oreilly.com/books/1234000001642) by J. David Eisenberg
* [Programming Elixir](https://pragprog.com/book/elixir/programming-elixir) by Dave Thomas
* [Programming Phoenix](https://pragprog.com/book/phoenix/programming-phoenix) by Chris McCord, Bruce Tate, and José Valim
* [Mastering Ruby Closures](https://pragprog.com/book/btrubyclo/mastering-ruby-closures) by Benjamin Tan Wei Hao
* [Working with Ruby Threads](https://pragprog.com/book/jsthreads/working-with-ruby-threads) by Jesse Storimer
* [Release It! Second Edition](https://pragprog.com/book/mnee2/release-it-second-edition) by Michael Nygard
* [Mastering Linux Network Administration](https://www.packtpub.com/networking-and-servers/mastering-linux-network-administration) by Jay LaCroix

__MOOCs:__

 * [Introduction to Linux](/assets/files/linux_certificate.pdf)
 * [X-Pack Security Course](https://www.elastic.co/training/x-pack-security)
 * [Introduction to Kubernetes](https://www.edx.org/course/introduction-kubernetes-linuxfoundationx-lfs158x)

<sub>these were free ¯\\\_(ツ)\_/¯</sub>

> The internet is scary. Consider keeping private thoughts to yourself.
